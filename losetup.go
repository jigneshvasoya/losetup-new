package losetup

import (
	"fmt"
	"os"
	"syscall"

	// #include <sys/sysmacros.h>
	"C"

	"golang.org/x/sys/unix"
)

func getFree() (int64, error) {
	ctrl, err := os.OpenFile(LoopControlPath, os.O_RDWR, 0660)
	if err != nil {
		return -1, fmt.Errorf("could not open %v: %v", LoopControlPath, err)
	}
	defer ctrl.Close()
	dev, _, errno := unix.Syscall(unix.SYS_IOCTL, ctrl.Fd(), CtlGetFree, 0)
	if dev < 0 {
		return -1, fmt.Errorf("could not get free device (err: %d): %v", errno, errno)
	}
	return int64(dev), nil
}

func Attach(backingFile, devicePath string, ro bool) error {
	flags := os.O_RDWR
	if ro {
		flags = os.O_RDONLY
	}

	back, err := os.OpenFile(backingFile, flags, 0660)
	if err != nil {
		return fmt.Errorf("could not open backing file: %v", err)
	}
	defer back.Close()

	minor, err := getFree()
	if err != nil {
		return err
	}

	// TODO: create device path if does not exists
	err = createDeviceFile(devicePath, Major, minor)
	if err != nil {
		return fmt.Errorf("failed to create device path %s, error: %v", devicePath, err)
	}

	loopFile, err := os.OpenFile(devicePath, flags, 0660)
	if err != nil {
		return fmt.Errorf("could not open loop device: %v", err)
	}
	defer loopFile.Close()

	_, _, errno := unix.Syscall(unix.SYS_IOCTL, loopFile.Fd(), SetFd, back.Fd())
	if errno == 0 {
		return nil
	}

	return nil
}

func Detach(devicePath string) error {
	if !exists(devicePath) {
		return fmt.Errorf("device path %s does not exists", devicePath)
	}

	loopFile, err := os.OpenFile(devicePath, os.O_RDONLY, 0660)
	if err != nil {
		return fmt.Errorf("could not open loop device")
	}
	defer loopFile.Close()

	_, _, errno := unix.Syscall(unix.SYS_IOCTL, loopFile.Fd(), ClrFd, 0)
	if errno != 0 {
		return fmt.Errorf("error clearing loopfile: %v", errno)
	}

	return nil

	return nil
}

func mkdev(mj int64, mi int64) uint32 {
	return uint32(C.gnu_dev_makedev(C.uint(mj), C.uint(mi)))
}

func exists(path string) bool {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		return false
	}

	return true
}

// create block device file
func createDeviceFile(path string, mj, mi int64) error {
	// remove previous path
	if exists(path) {
		os.Remove(path)
	}

	mode := syscall.S_IFBLK | 0640
	dev := mkdev(mj, mi)

	//fmt.Printf("dev: %d, major: %d, minor: %d\n", dev, mj, mi)

	if err := syscall.Mknod(path, uint32(mode), int(dev)); err != nil {
		return err
	}

	return nil
}

