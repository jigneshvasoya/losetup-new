package losetup

import (
	"testing"
	"fmt"
)

const (
	imagePath	= "/tmp/test.img"
	dev		= "/dev/abc"
)

func TestLosetup(t *testing.T) {
	if err := Attach(imagePath, dev, false); err != nil {
		fmt.Printf("error: %v\n", err)
	}

	/*
	if err := Detach(imagePath); err != nil {
		fmt.Printf("error: %v\n", err)
	}*/
}

